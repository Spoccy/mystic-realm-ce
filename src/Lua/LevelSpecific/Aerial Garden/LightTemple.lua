--Temple of Light in Starlight Palace
local portalopener = 0


local function keystone1(line, mo, d)
	if (mrce_hyperstones & (1 << (0))) then
		local em1 = P_SpawnMobj(-184*FRACUNIT, -200*FRACUNIT, 3912*FRACUNIT, MT_LIGHTNING_STONE)
		portalopener = $ + 1
		S_StartSoundAtVolume(em1, sfx_s3k9c, 150)
		--print("GO")
	else
		--print("NO")
	end
end

addHook("LinedefExecute", keystone1, "KEYSTNE1")

local function keystone2(line, mo, d)
	if (mrce_hyperstones & (1 << (1))) then
		local em2 = P_SpawnMobj(0*FRACUNIT, -200*FRACUNIT, 3912*FRACUNIT, MT_FIRE_STONE)
		portalopener = $ + 1
		S_StartSoundAtVolume(em2, sfx_s3k9c, 150)
		--print("GO")
	else
		--print("NO")
	end
end

addHook("LinedefExecute", keystone2, "KEYSTNE2")

local function keystone3(line, mo, d)
	if (mrce_hyperstones & (1 << (2))) then
		local em3 = P_SpawnMobj(184*FRACUNIT, -200*FRACUNIT, 3912*FRACUNIT, MT_WATER_STONE)
		portalopener = $ + 1
		S_StartSoundAtVolume(em3, sfx_s3k9c, 150)
		--print("GO")
	else
		--print("NO")
	end
end

addHook("LinedefExecute", keystone3, "KEYSTNE3")

addHook("ThinkFrame", function()
	if mapheaderinfo[gamemap].lvlttl == "Starlight Palace" then
		--print(mrce_hyperstones)
	end
	--mrce_hyperstones = 7
	if mrce_hyperstones == 7 then
		P_LinedefExecute(12)
	else
		--print(portalopener)
	end
end)

addHook("NetVars", function(net)
	portalopener = net($)
end)