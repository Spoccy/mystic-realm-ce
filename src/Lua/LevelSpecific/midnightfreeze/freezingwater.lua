hud.add(function(d, p)
	local TOP_LEFT = d.cachePatch("FROZOVR1")
	local TOP_RIGHT = d.cachePatch("FROZOVR2")
	local BOTTOM_LEFT = d.cachePatch("FROZOVR3")
	local BOTTOM_RIGHT = d.cachePatch("FROZOVR4")
	local TRANSPARENCY = 0
	
	if (p.mo.eflags & MFE_UNDERWATER) and (P_InSpaceSector(p.mo)) then
		if p.powers[pw_spacetime] >= 10*TICRATE
			TRANSPARENCY = V_90TRANS
		elseif p.powers[pw_spacetime] >= 9*TICRATE
			TRANSPARENCY = V_80TRANS
		elseif p.powers[pw_spacetime] >= 8*TICRATE
			TRANSPARENCY = V_70TRANS
		elseif p.powers[pw_spacetime] >= 7*TICRATE
			TRANSPARENCY = V_60TRANS
		elseif p.powers[pw_spacetime] >= 6*TICRATE
			TRANSPARENCY = V_50TRANS
		elseif p.powers[pw_spacetime] >= 5*TICRATE
			TRANSPARENCY = V_40TRANS
		elseif p.powers[pw_spacetime] >= 4*TICRATE
			TRANSPARENCY = V_30TRANS
		elseif p.powers[pw_spacetime] >= 3*TICRATE
			TRANSPARENCY = V_20TRANS
		elseif p.powers[pw_spacetime] >= 2*TICRATE
			TRANSPARENCY = V_10TRANS
		end
		
		d.draw(0, 0, TOP_LEFT, V_SNAPTOTOP|V_SNAPTOLEFT|TRANSPARENCY|V_PERPLAYER)
		d.draw(320, 0, TOP_RIGHT, V_SNAPTOTOP|V_SNAPTORIGHT|TRANSPARENCY|V_PERPLAYER)
		d.draw(0, 200, BOTTOM_LEFT, V_SNAPTOBOTTOM|V_SNAPTOLEFT|TRANSPARENCY|V_PERPLAYER)
		d.draw(320, 200, BOTTOM_RIGHT, V_SNAPTOBOTTOM|V_SNAPTORIGHT|TRANSPARENCY|V_PERPLAYER)
	end
end, "game")