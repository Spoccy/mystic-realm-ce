/*
MRCE Lua HUD

(C) 2020-2022 by K. "ashifolfi" J.
*/

local slidein = "no"
local hticker = 0

local function HudToggle(player, arg)
    if arg and player and player.mrce
        if arg == "1" or arg == "on" or arg == "default" or arg == "true" or arg == "normal" or arg == "yes"
			player.mrce.hud = 1
			if player.playerstate == PST_LIVE then
				CONS_Printf(player, "MRCE Custom hud enabled")
			end
			if io and player == consoleplayer
				local file = io.openlocal("client/mrce/hud.dat", "w+")
				file:write(arg)
				file:close()
			end
		elseif arg == "off" or arg == "0" or arg == "no" or arg == "false" or arg == "disable"
			player.mrce.hud = 0
			if player.playerstate == PST_LIVE then
				CONS_Printf(player, "MRCE Custom hud disabled")
			end
			if io and player == consoleplayer
				local file = io.openlocal("client/mrce/hud.dat", "w+")
				file:write(arg)
				file:close()
			end
        end
    elseif player
        CONS_Printf(player, "Toggle MRCE lua hud. Note many effects are used by the custom hud, and are lost when disabled, so BEWARE")
    end
end

COM_AddCommand("mr_hud", function(player, arg)
	HudToggle(player, arg)
end)

addHook("PlayerSpawn", function(p)
	if io and p == consoleplayer and p.howfast == nil
		local file = io.openlocal("client/mrce/hud.dat")
		if file
			local string = file:read("*a")
			if string == "1" or string == "on" or string == "default" or string == "normal" or string == "yes" or string == nil then
				COM_BufInsertText(p, "mr_hud 1")
			elseif string == "0" or string == "disable" or string == "off" or string == "no" then
				COM_BufInsertText(p, "mr_hud 0")
			end
			file:close()
		else
			COM_BufInsertText(p, "mr_hud 1")
		end
	end
end)

local function DrawMRCEHUD(v, p, cam, ticker, endticker)
	if p.spectator return false end
	if not p.realmo return false end
	if p.mrce.hud == 0
		hud.enable("rings")
		hud.enable("time")
		hud.enable("score")
		return 
	end

	if (p.realmo) and not (p.mo.skin == "modernsonic") and not (p.mo.skin == "speccy" and p.speccy and p.speccy.roguelike and p.speccy.modes[2][2]) and not ((p.mo.skin == "samus") or (p.mo.skin == "basesamus")) and not (p.mo.skin == "duke") and not (srb2p) and not (maptol & TOL_NIGHTS) and not (G_IsSpecialStage(gamemap)) and gamemap != 99 and CHUD == nil and customhud == nil
	and p == displayplayer and p.mrce.hud == 1 then
		v.draw(20, 10, v.cachePatch("MRHSCORE"), V_SNAPTOLEFT|V_SNAPTOTOP|V_PERPLAYER, v.getColormap(p.realmo.skin, p.realmo.color))
		v.draw(20, 26, v.cachePatch("MRHTIME"), V_SNAPTOLEFT|V_SNAPTOTOP|V_PERPLAYER, v.getColormap(p.realmo.skin, p.realmo.color))
		if not ultimatemode
		or mapheaderinfo[gamemap].lvlttl == "Dimension Warp"
			v.draw(20, 42, v.cachePatch("MRHRINGS"), V_SNAPTOLEFT|V_SNAPTOTOP|V_PERPLAYER, v.getColormap(p.realmo.skin, p.realmo.color))
			DrawMotdString(v, 75*FRACUNIT, 44*FRACUNIT, FRACUNIT, tostring(p.rings), "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS)
		end
		DrawMotdString(v, 75*FRACUNIT, 9*FRACUNIT, FRACUNIT, tostring(p.score), "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS)
		if G_TicsToMinutes(p.realtime) < 10 and mapheaderinfo[gamemap].lvlttl != "Dimension Warp"
			DrawMotdString(v, 75*FRACUNIT, 27*FRACUNIT, FRACUNIT, "0", "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS)
			DrawMotdString(v, 83*FRACUNIT, 27*FRACUNIT, FRACUNIT, G_TicsToMinutes(p.realtime), "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS)
		elseif G_TicsToMinutes(p.realtime) >= 10 and mapheaderinfo[gamemap].lvlttl != "Dimension Warp"
			DrawMotdString(v, 75*FRACUNIT, 27*FRACUNIT, FRACUNIT, G_TicsToMinutes(p.realtime), "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS)
		elseif mapheaderinfo[gamemap].lvlttl == "Dimension Warp"
			DrawMotdString(v, 75*FRACUNIT, 27*FRACUNIT, FRACUNIT, tostring(p.warpminutes), "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS)
		end
		if G_TicsToSeconds(p.realtime) < 10 and mapheaderinfo[gamemap].lvlttl != "Dimension Warp"
			DrawMotdString(v, 99*FRACUNIT, 27*FRACUNIT, FRACUNIT, "0", "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS)
			DrawMotdString(v, 107*FRACUNIT, 27*FRACUNIT, FRACUNIT, G_TicsToSeconds(p.realtime), "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS, 1)
		elseif G_TicsToSeconds(p.realtime) >= 10 and mapheaderinfo[gamemap].lvlttl != "Dimension Warp"
			DrawMotdString(v, 99*FRACUNIT, 27*FRACUNIT, FRACUNIT, G_TicsToSeconds(p.realtime), "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS, 1)
		elseif mapheaderinfo[gamemap].lvlttl == "Dimension Warp"
			DrawMotdString(v, 99*FRACUNIT, 27*FRACUNIT, FRACUNIT, tostring(p.warpseconds), "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS, 1)
		end
		if G_TicsToCentiseconds(p.realtime) < 10 and mapheaderinfo[gamemap].lvlttl != "Dimension Warp"
			DrawMotdString(v, 123*FRACUNIT, 27*FRACUNIT, FRACUNIT, "0", "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS)
			DrawMotdString(v, 131*FRACUNIT, 27*FRACUNIT, FRACUNIT, G_TicsToCentiseconds(p.realtime), "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS, 1)
		elseif G_TicsToCentiseconds(p.realtime) >= 10 and mapheaderinfo[gamemap].lvlttl != "Dimension Warp"
			DrawMotdString(v, 123*FRACUNIT, 27*FRACUNIT, FRACUNIT, G_TicsToCentiseconds(p.realtime), "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS, 1)
		elseif mapheaderinfo[gamemap].lvlttl == "Dimension Warp"
			DrawMotdString(v, 123*FRACUNIT, 27*FRACUNIT, FRACUNIT, tostring(p.warpcentiseconds), "MRCEFNT", V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS, 1)
		end
		v.draw(91, 27, v.cachePatch("MRCEFNTS"), V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS)
		v.draw(115, 27, v.cachePatch("MRCEFNTP"), V_SNAPTOTOP|V_SNAPTOLEFT|V_PERPLAYER|V_HUDTRANS)
		if (p.rings == 0 and (leveltime/5%2) and not ultimatemode) or (mapheaderinfo[gamemap].lvlttl == "Dimension Warp" and p.rings <= 10 and (leveltime/5%2))
			v.draw(20, 42, v.cachePatch("MRHRRING"), V_SNAPTOLEFT|V_SNAPTOTOP|V_PERPLAYER, v.getColormap(p.realmo.skin, p.realmo.color))
		end
		if p.mrce.hud == 1 then
			hud.disable("rings")
			hud.disable("time")
			hud.disable("score")
		end
	end
end

hud.add(DrawMRCEHUD, "game")

addHook("PlayerThink", function(p)
	if (mapheaderinfo[gamemap].lvlttl != "Dimension Warp") return end
	if not (leveltime%35)
	or (leveltime%TICRATE == TICRATE/5) or (leveltime%TICRATE == ((TICRATE/5) * 3)) or (leveltime%TICRATE == ((TICRATE/5) * 2)) or (leveltime%TICRATE == ((TICRATE/5) * 4))
		p.warpminutes =  P_RandomRange(10, 99)
		p.warpseconds =  P_RandomRange(10, 99)
		p.warpcentiseconds =  P_RandomRange(10, 99)
	end
end)
