--manage second quest stuff. mostly triggering linedefs and swapping textures and such, might add encore support later too
--obviously this is all just here to be here, not expecting anyone to use it yet lol, though due to the nature of how the levels are to be built for this, it might be something to keep in mind

addHook("MapLoad", function(p, v)
	if secondquest == true and gamemap > 100 and gamemap < 168 then  --if second quest is active, saved in my misc script since luabanks
		for i = 3000, 3050, 1 do  --reserve linedef tags 7000 through 7050 for executing effects to change the level layout on mapload. Should hopefully be more than enough
			P_LinedefExecute(i)
		end
	elseif gamemap > 100 and gamemap < 168 then --we're on main quest instead, do the same for effects that shouldn't occur in second quest
		for i = 2000, 2050, 1 do
			P_LinedefExecute(i)
		end
	end
end)