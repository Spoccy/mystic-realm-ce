local debug = 0
freeslot(
	"SPR_ANGL",
	"SPR_TFLR",
	"MT_ANGEL",
	"S_ANGEL1",
	"S_ANGEL2",
	"S_ANGEL3",
	"S_ANGEL4",
	"A_AngelThink",
	"sfx_grchm",
	"sfx_fwarn",
	"sfx_iwarn"
)

addHook("MapLoad", do
	for player in players.iterate
		player.mo.angel = nil
	end
end)

function A_AngelThink(actor, var1, var2)
	if actor.target == nil
		//Search for airborne players in radius who don't have an angel chasing them yet
		for player in players.iterate
			if player.spectator return false end
			if not player.realmo return false end
			local search_distance = P_AproxDistance(actor.x - player.mo.x, actor.y - player.mo.y) / FRACUNIT
			local search_height = abs(actor.z - player.mo.z) / FRACUNIT
			if player.playerstate == PST_LIVE and not P_IsObjectOnGround(player.mo) and search_height <= 450 and search_distance <= 1200 and player.mo.angel == nil
				actor.target = player.mo
				actor.target.angel = actor
				actor.kill_timer = 260
				
				actor.spawnx = actor.x
				actor.spawny = actor.y
				actor.spawnz = actor.z
				S_StartSoundAtVolume(actor.target, sfx_grchm, 255, actor.target.player)
				S_FadeMusic(0, 4*MUSICRATE, actor.target.player)
				if actor.target.player.earsave <= 0
					S_StartSoundAtVolume(actor.target, sfx_iwarn, 255, actor.target.player)
					actor.target.player.earsave = 105
				end
				S_FadeMusic(0, 4*MUSICRATE, actor.target.player)
			end
		end
	else //Already have a target
		if P_IsObjectOnGround(actor.target)
			//Target reached the ground and is safe
			if actor.target.player.playerstate == PST_LIVE
				S_StopFadingMusic(actor.target.player)
				S_FadeMusic(100, 1*MUSICRATE, actor.target.player)
				for i = 0, 10
					S_StopSoundByID(actor.target, sfx_iwarn)
					S_StopSoundByID(actor.target, sfx_fwarn)
					S_StopSoundByID(actor.target, sfx_grchm)
				end
				S_StartSoundAtVolume(actor.target, 445, 200, actor.target.player)
				actor.target.player.finalwarning = false
				if actor.target.player.screenflash == true
					P_FlashPal(actor.target.player, PAL_WHITE, 1)
				end
			end
			actor.target.angel = nil
			actor.target = nil
			actor.kill_timer = 260
			P_TeleportMove(actor, actor.spawnx, actor.spawny, actor.spawnz)
			actor.momx = 0
			actor.momy = 0
			actor.momz = 0
		
		else //Chase target and countdown
			actor.kill_timer = actor.kill_timer - 1
			if actor.target.player.fly1 > 8 and (actor.target.player.powers[pw_tailsfly] > 0) and actor.kill_timer >= 27
				actor.kill_timer = actor.kill_timer - 10
			end
			local tx = actor.target.x
			local ty = actor.target.y
			local tz = actor.target.z
			local chase_distance = P_AproxDistance(actor.x - actor.target.x, actor.y - actor.target.y) / FRACUNIT
			local chase_height = abs(actor.z - actor.target.z) / FRACUNIT
			local chase_amt = 1*FRACUNIT
			local chase_max = 18
			
			if chase_distance > 60 or chase_height > 60
				chase_amt = 4*FRACUNIT
			end
			if chase_distance > 200 or chase_height > 200
				chase_amt = 8*FRACUNIT
			end
			
			if actor.x < tx
				actor.momx = min(actor.momx + chase_amt, chase_max * FRACUNIT)
			end
			if actor.x > tx
				actor.momx = min(actor.momx - chase_amt, chase_max * FRACUNIT)
			end
			if actor.y < ty
				actor.momy = min(actor.momy + chase_amt, chase_max * FRACUNIT)
			end
			if actor.y > ty
				actor.momy = min(actor.momy - chase_amt, chase_max * FRACUNIT)
			end
			if actor.z < tz
				actor.momz = min(actor.momz + chase_amt, chase_max * FRACUNIT)
			end
			if actor.z > tz
				actor.momz = min(actor.momz - chase_amt, chase_max * FRACUNIT)
			end
			
			if debug
				print(actor.kill_timer)
			end
			
			if actor.kill_timer < 26 and not actor.target.player.finalwarning
				S_StartSound(actor.target, sfx_fwarn)
				actor.target.player.finalwarning = true
			end
			
			if actor.kill_timer <= 0 //Time's up
				actor.target.player.finalwarning = false
				P_DamageMobj(actor.target, actor, actor, 1, DMG_SPACEDROWN)
				S_StartSoundAtVolume(actor.target, 399, 180, actor.target.player)
				if actor.target.player.screenflash == true
					P_FlashPal(actor.target.player, PAL_WHITE, 5)
				end
				actor.target.angel = nil
				actor.target = nil
				actor.kill_timer = 260
				P_TeleportMove(actor, actor.spawnx, actor.spawny, actor.spawnz)
				actor.momx = 0
				actor.momy = 0
				actor.momz = 0
			end
		end
	end
end

addHook("PlayerThink", function(p)
	p.earsave = $ or 0
	if p.earsave > 0
		p.earsave = $ - 1
	end
	--print(p.earsave)
end)

sfxinfo[sfx_grchm] = {
  caption = "Ominous Chiming"
}

mobjinfo[MT_ANGEL] = {
	doomednum = 3108,
	spawnstate = S_ANGEL1,
	spawnhealth = 1000,
	flags = MF_NOGRAVITY,
	radius = 16*FRACUNIT,
	height = 16*FRACUNIT
}

states[S_ANGEL1] = {
	sprite = SPR_ANGL,
	frame = TR_TRANS30|A,
	tics = 1,
	action = A_AngelThink,
	nextstate = S_ANGEL2
}

states[S_ANGEL2] = {
	sprite = SPR_ANGL,
	frame = TR_TRANS30|A,
	tics = 1,
	action = A_AngelThink,
	nextstate = S_ANGEL3
}

states[S_ANGEL3] = {
	sprite = SPR_ANGL,
	frame = TR_TRANS30|B,
	tics = 1,
	action = A_AngelThink,
	nextstate = S_ANGEL4
}

states[S_ANGEL4] = {
	sprite = SPR_ANGL,
	frame = TR_TRANS30|B,
	tics = 1,
	action = A_AngelThink,
	nextstate = S_ANGEL1
}