////////////////////////////////////////////////////////////////////////////////////////////////////////////
//																				                		  //
// Iciclivore enemy. Lua by Radicalicious, freezing mechanic by Lat', sprites for the gas by Spectorious. //
//																				                		  //
////////////////////////////////////////////////////////////////////////////////////////////////////////////



// Freeslots

freeslot("MT_ICICLIVORE")
freeslot("S_ICICLIVORE_LOOK", "S_ICICLIVORE_AWAKEN1", "S_ICICLIVORE_AWAKEN2", "S_ICICLIVORE_AWAKEN3", "S_ICICLIVORE_GAS1", "S_ICICLIVORE_GAS2",
"S_ICICLIVORE_GAS3", "S_ICICLIVORE_GAS4", "S_ICICLIVORE_GAS5", "S_ICICLIVORE_GASREPEAT", "S_ICICLIVORE_CLOSE1", "S_ICICLIVORE_CLOSE2")
freeslot("SPR_ICAN")

// Objects

mobjinfo[MT_ICICLIVORE] = {
		//$Name "Iciclivore"
		//$Sprite ICANA0
		//$Category "Mystic Realm - Midnight Freeze Zone"
		2701,            // doomednum
		S_ICICLIVORE_LOOK,  // spawnstate
		1,              // spawnhealth
		S_ICICLIVORE_AWAKEN1,   // seestate
		sfx_None,       // seesound
		0,              // reactiontime
		sfx_None,       // attacksound
		S_NULL,         // painstate
		0,              // painchance
		sfx_None,       // painsound
		S_NULL,         // meleestate
		S_NULL,         // missilestate
		S_XPLD_FLICKY,  // deathstate
		S_NULL,         // xdeathstate
		sfx_pop,        // deathsound
		0,              // speed
		12*FRACUNIT,    // radius
		80*FRACUNIT,    // height
		0,              // display offset
		100,            // mass
		0,              // damage
		sfx_None,       // activesound
		MF_SPECIAL|MF_SHOOTABLE|MF_ENEMY|MF_SPAWNCEILING|MF_NOGRAVITY, // flags
		S_NULL          // raisestate
}
	


// States

states[S_ICICLIVORE_LOOK] = {SPR_ICAN, 0, 5, A_Look, 1200*FRACUNIT+1, 1, S_ICICLIVORE_LOOK}
states[S_ICICLIVORE_AWAKEN1] = {SPR_ICAN, 0, 3, A_PlaySound, sfx_s3k76, 1, S_ICICLIVORE_AWAKEN2}
states[S_ICICLIVORE_AWAKEN2] = {SPR_ICAN, 1, 5, nil, 0, 0, S_ICICLIVORE_AWAKEN3}
states[S_ICICLIVORE_AWAKEN3] = {SPR_ICAN, 2, 8, nil, 0, 0, S_ICICLIVORE_GAS1}
states[S_ICICLIVORE_GAS1] = {SPR_ICAN, 2, 15, A_PlaySound, sfx_s3k93, 1, S_ICICLIVORE_GAS2}
states[S_ICICLIVORE_GAS2] = {SPR_ICAN, 1, 4,  nil, 0, 0, S_ICICLIVORE_GAS3}
states[S_ICICLIVORE_GAS3] = {SPR_ICAN, 2, 0,  A_PlaySound, sfx_s3k97, 1, S_ICICLIVORE_GAS4}
states[S_ICICLIVORE_GAS4] = {SPR_ICAN, 2, 5,  A_CanarivoreGas, MT_CANARIVORE_GAS, 0, S_ICICLIVORE_GAS5}
states[S_ICICLIVORE_GAS5] = {SPR_ICAN, 1, 5,  nil, 0, 0, S_ICICLIVORE_GASREPEAT}
states[S_ICICLIVORE_GASREPEAT] = {SPR_ICAN, 2, 0, A_Repeat, 6, S_CANARIVORE_GAS4, S_ICICLIVORE_CLOSE1}
states[S_ICICLIVORE_CLOSE1] = {SPR_ICAN, 1, 8,  nil, 0, 0, S_ICICLIVORE_CLOSE2}
states[S_ICICLIVORE_CLOSE2] = {SPR_ICAN, 0, 90, nil, sfx_s3k5d, 1, S_ICICLIVORE_LOOK}
